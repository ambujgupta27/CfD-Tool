import pandas as pd
import numpy as np
import numpy_financial as npf
import datetime
from sklearn.linear_model import LinearRegression
from scipy.optimize import fsolve
from scipy.optimize import differential_evolution
from scipy import optimize
import os
import math



def lf_scaling(lf_series, target_lf):
    avg_lf_series = lf_series.mean()
    new_series = lf_series
    max_err = 0.0001 #"Maximum deviation of root from 1.0."
    root = 1
    iter_count = 0
    err = math.log(target_lf)/math.log(avg_lf_series)
    while (iter_count < 100 and abs(err-1) > max_err):
        root = root * err
        new_series = new_series.pow(root)
        err = math.log(target_lf)/math.log(avg_lf_series)
        iter_count = iter_count + 1
    return new_series


    

# import numpy_financial as npf

def monthly_optimization(strike, start_month, start_year):
    # 1. |INPUTS|

    # Inputs: strike = CfD strike price Inputs: start_month = first month of year in which CfD is operational,
    # indexed 1 (ie. May = 5) Inputs: start_year = first year in which CfD is operational, eg. 2022 HourlyInput
    # should contain hourly prices and hourly load factor together with timestamp YearlyInput should contain share of
    # generation under CfD on an annual basis- can be labelled by year but the code takes first item to correspond to
    # start_year anyway Outputs are produced at Yearly granularity through yearly_out and monthly granularity through
    # monthly_out as csv files

    start_month = start_month - 1
    # avoids having to have the start month zero indexed
    # strike is the CfD strike price
    ###hourly = pd.read_csv("C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/HourlyInput.csv")
    hourly = pd.read_csv("inputs/HourlyInput_lOW_solar.csv")
    # read hourly input file
    # hourly should be a csv with a timestamp string titled timestamp, wholesale price titled price and load factor title lf
    hourly['Timestamp'] = pd.to_datetime(hourly['Timestamp'])
    hourly['Date'] = hourly['Timestamp'].dt.floor('d')


    control_CfD = pd.read_csv('control_CfD.csv', index_col=0, header=0)

    if control_CfD.loc['rescale_lf']['Value']:
        hourly = hourly.set_index('Timestamp')
        client_lf = pd.read_csv('client_yearly_avg_lf.csv', index_col=0, header=0)
        for yr in range((hourly['Date'].dt.year).min(), (hourly['Date'].dt.year).max()):
            lf_series = hourly[str(yr)]['LF']
            target_lf = client_lf.loc[yr]
            new_series = lf_scaling(lf_series, target_lf)
            hourly.loc[new_series.index] = new_series[0]
        hourly.reset_index(inplace=True)

    # convert the timestamp string to a datetime object, add a date column
    hourly['Revenue'] = hourly['Price'] * hourly['LF']
    #yearly = pd.read_csv("C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/YearlyInput.csv")
    #yearly = pd.read_csv("C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/YearlyInput 2023.csv")
    yearly = pd.read_csv("inputs/YearlyInput_monthly.csv")

    # read yearly input file
    # yearly should contain year in  a column title Year and a generation under the CfD titled share - should be given as a ratio of p50 generation

    # 2. |SIMPLE CONVERSIONS|

    daily = hourly.groupby(['Date']).sum()
    # creates the daily values
    daily['Price'] = daily['Price'] / 24
    # convert the sum to an average daily price - TGE base
    daily['Capture'] = daily['Revenue'] / daily['LF']
    # creates the daily capture price
    daily = daily.reset_index()
    daily['CfD_bonus'] = strike - daily['Price']
    # move date to column, set up a column with the CfD bonus per mwh value, ie. strike price minus base price
    start_date = str(start_year) + "-01-01"
    rng = pd.date_range(start_date, periods=(12 * 16), freq='M')
    monthly = pd.DataFrame({'Date': rng})

    # 3. |MONTHLY OPTIMIZATION|

    for measure_i in np.arange(0, 11):
        monthly['Generation' + str(measure_i)] = 0.0
        monthly['CfD_generation' + str(measure_i)] = 0.0
        monthly['CfD_revenue' + str(measure_i)] = 0.0
        monthly['merchant_generation' + str(measure_i)] = 0.0
        monthly['merchant_revenue' + str(measure_i)] = 0.0
        monthly['effective_capture' + str(measure_i)] = 0.0
        for year_count in np.arange(0, 16):
            # calculate over 16 years so that the correct months from the first and last years can be selected and the CfD period precisely covered
            year = daily[daily['Date'].dt.year == year_count + start_year]
            # loop through CfD duration years, taking that year from daily dataframe each time
            for month_i in np.arange(0, 12):
                month_count = month_i + (year_count * 12)
                # bit budget but month_count is total months since start, month_i is month in a given year
                month = year[year['Date'].dt.month == month_i + 1]
                month = month.sort_values(by=['Price'])
                month.reset_index()
                # loop through months in year. each time year and month are just used in the given iteration
                generation = month['LF'].sum()
                # total generation by the asset in the month
                monthly.at[month_count, 'Generation' + str(measure_i)] = generation
                capture_revenue = 0.0
                for i in month.index:
                    if monthly['CfD_generation' + str(measure_i)][month_count] < 0.85 * \
                            yearly['Share' + str(measure_i)][year_count] * generation:
                        if monthly['CfD_generation' + str(measure_i)][month_count] + month['LF'][i] < yearly['Share' + str(measure_i)][year_count] * generation:
                        # if monthly['CfD_generation'][month_count] < 0.85*year_generation_CfD/12:
                        # which if clause you choose defines whether you deliver a set volume to the CfD or a set share of your production within the month
                            monthly.at[month_count, 'CfD_generation' + str(measure_i)] += month['LF'][i]
                            monthly.at[month_count, 'CfD_revenue' + str(measure_i)] += (month['Capture'][i] +
                                                                                        month['CfD_bonus'][i]) * \
                                                                                       month['LF'][i]
                            capture_revenue += month['Revenue'][i]
                        elif monthly['CfD_generation' + str(measure_i)][month_count] + month['LF'][i] < yearly['Share' + str(measure_i)][
                            year_count] * generation:
                            if month['CfD_bonus'][i] > 0:
                                monthly.at[month_count, 'CfD_generation' + str(measure_i)] += month['LF'][i]
                                monthly.at[month_count, 'CfD_revenue' + str(measure_i)] += (month['Capture'][i] +
                                                                                            month['CfD_bonus'][i]) * \
                                                                                           month['LF'][i]
                                capture_revenue += month['Revenue'][i]

                monthly.at[month_count, 'merchant_generation' + str(measure_i)] = \
                monthly['Generation' + str(measure_i)][month_count] - \
                monthly['CfD_generation' + str(measure_i)][month_count]
                monthly.at[month_count, 'merchant_revenue' + str(measure_i)] = month['Revenue'].sum() - capture_revenue
                monthly.at[month_count, 'effective_capture' + str(measure_i)] = (monthly[
                                                                                     'CfD_revenue' + str(measure_i)][
                                                                                     month_count] +
                                                                                 monthly['merchant_revenue' + str(
                                                                                     measure_i)][month_count]) / \
                                                                                monthly['Generation' + str(measure_i)][
                                                                                    month_count]

    # 4. |RESULTS PUBLISHING|

    monthly_out = monthly.loc[start_month:(start_month + 12 * 15 - 1)].copy()
    # select the correct months from the calculation for output
    monthly_out['Year'] = monthly_out['Date'].dt.year
    for measure_i in np.arange(0, 11):
        monthly_out['Total_revenue' + str(measure_i)] = monthly_out['CfD_revenue' + str(measure_i)] + monthly_out[
            'merchant_revenue' + str(measure_i)]
        # calculate revenue, effective capture price and merchant/cfd generation split on monthly basis
    yearly_out = monthly_out.groupby(['Year']).sum()
    for measure_i in np.arange(0, 11):
        yearly_out['effective_capture' + str(measure_i)] = yearly_out['Total_revenue' + str(measure_i)] / yearly_out[
            'Generation' + str(measure_i)]
        # calculate outputs at yearly granularity
    print(yearly_out)
    yearly_out.to_csv('op_yr_try1.csv')
    #yearly_out.to_csv('outputs_igneo/yearly_out_central.csv')
    #yearly_out.to_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/yearly_out_2023.csv')
    #yearly_out.to_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/Outputs/1.3/yearly_out_2023.csv')
    monthly_out.to_csv('op_mn_try1.csv')
    #monthly_out.to_csv('outputs_igneo/monthly_out.csv')
    #monthly_out.to_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/Outputs/1.3/monthly_out_2023.csv')
    # output to csv files


def result_lin():
    # needs csvs in yearly out format for central and low scenario
    # reads them in and linearises
    #path = r'C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final'

    #filename1 = 'yearly_out_low.csv'
    #filename2 = 'yearly_out_central.csv'

    #yearly_out_low = pd.read_excel(os.path.join(path, filename1), engine='openpyxl', sheet_name='yearly_out_low')
    #yearly_out_central = pd.read_excel(os.path.join(path, filename2), engine='openpyxl', sheet_name='yearly_out_central')
    yearly_out_low = pd.read_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/yearly_out_low_2023.csv')
    yearly_out_central = pd.read_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/yearly_out_2023.csv')

    annual_generation = yearly_out_low['Generation0'][0]

    # requires running script twice then renaming output. once to create yearly_out_low and once to create yearly_out_central

    cfd_low = pd.DataFrame()
    cfd_central = pd.DataFrame()
    merchant_low = pd.DataFrame()
    merchant_central = pd.DataFrame()

    for i in np.arange(0, 11):
        cfd_low['revenue' + str(i)] = yearly_out_low['CfD_revenue' + str(i)]
        cfd_central['revenue' + str(i)] = yearly_out_central['CfD_revenue' + str(i)]
        merchant_low['revenue' + str(i)] = yearly_out_low['merchant_revenue' + str(i)]
        merchant_central['merchant_revenue' + str(i)] = yearly_out_central['merchant_revenue' + str(i)]

    cfd_low_lin = linearise(cfd_low)
    cfd_central_lin = linearise(cfd_central)
    merchant_low_lin = linearise(merchant_low)
    merchant_central_lin = linearise(merchant_central)
    # print(merchant_central_lin)
    return cfd_low_lin, cfd_central_lin, merchant_low_lin, merchant_central_lin, annual_generation


def linearise(input):
    # input should be data frame rows = years column = cfd share
    # output is gradient and intercept of line fit
    # the fact this function is called linearise is a relic of previous approach. We fit the data to a third-order polynomial
    # as lower-order approaches don't provide sufficient accuracy to capture effect of cfd optimisation and
    # mean that revenues at fully-merchant or fully-cfd setups are overestimated

    X = np.arange(0, 1.1, 0.1)
    parameters = pd.DataFrame()

    for year in np.arange(0, 15):
        Y = input.iloc[year].values
        # print(Y)
        c, a, m, b = np.polyfit(X, Y, 3)
        parameters.at[year, 'third_gradient'] = c
        parameters.at[year, 'second_gradient'] = a
        parameters.at[year, 'gradient'] = m
        parameters.at[year, 'intercept'] = b
        # Input checker of r-squared values for these
    return parameters

def equity_irr_calculation_setup(share_timeline, start_year, cfd_dscr, debt_term, cod, p90_ratio, lifetime, capex):
    # separate out the repeatable parts of the irr calculation to here to avoid repeating work when optimising share
    # start year should be the same as for monthly optimisation - avoiding introducing a start month at the moment for simplicity
    # cfd_dscr is the debt service coverage ratio applied to cfd revenue component
    # debt_term is lenght of debt repayment
    # cod is cost of debt
    # p90 ratio is share of p50 generation under p90 generation scenario - may not be used here initially
    # lifetime is the operating lifetime of the asset

    cfd_low_lin, cfd_central_lin, merchant_low_lin, merchant_central_lin, annual_generation = result_lin()
    cost_input = pd.read_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/cost_input.csv')
    merchant_dscr_input = pd.read_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/merchant_dscr_input.csv')
    merchant_input = pd.read_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/merchant_input.csv')
    # print(merchant_dscr_input)
    cashflow_tail = pd.DataFrame()
    cashflow_tail['central_revenue'] = merchant_input.iloc[(15 + (1)):(lifetime + (1)),1] * annual_generation
    cashflow_tail['costs'] = cost_input.iloc[15:lifetime + 1, 1]
    cashflow_tail.reset_index(drop=True)

    # cashflow during the merchant tail period
    # setup assumes debt period doesn't exceed 15 years - rather reasonable

    equity_irr = equity_irr_calculation_flat(share_timeline, start_year, cfd_dscr, debt_term, cod, p90_ratio, lifetime,
                                             capex, cfd_low_lin, cfd_central_lin, merchant_low_lin,
                                             merchant_central_lin, annual_generation, cost_input, merchant_dscr_input,
                                             merchant_input, cashflow_tail)
    return equity_irr


def timeline_optimization(start_year, cfd_dscr, debt_term, cod, p90_ratio, lifetime, capex):
    # similar to irr calculation but instead of taking share timeline as an input finds the optimum

    cfd_low_lin, cfd_central_lin, merchant_low_lin, merchant_central_lin, annual_generation = result_lin()
    cost_input = pd.read_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/cost_input.csv')
    merchant_dscr_input = pd.read_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/merchant_dscr_input.csv')
    merchant_input = pd.read_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/merchant_input.csv')

    cashflow_tail = pd.DataFrame()
    cashflow_tail['central_revenue'] = merchant_input.iloc[(15 + (1)):(lifetime + (1)),1] * annual_generation
    cashflow_tail['costs'] = cost_input.iloc[15:lifetime + 1, 1]
    cashflow_tail.reset_index(drop=True)
    # cashflow during the merchant tail period
    # setup assumes debt period doesn't exceed 15 years - rather reasonable
    share_timeline = np.array([0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5])
    # simply to initialise the share_timeline array
    lb = np.zeros(15)
    ub = np.ones(15)
    # bounds set shares to be between zero and one
    share_bounds = optimize.Bounds(lb, ub)
    share_timeline = differential_evolution(equity_irr_calculation_flat, bounds=share_bounds, args=(start_year, cfd_dscr, debt_term, cod, p90_ratio, lifetime, capex, cfd_low_lin, cfd_central_lin, merchant_low_lin,merchant_central_lin, annual_generation, cost_input, merchant_dscr_input, merchant_input, cashflow_tail), tol=0.001)
    # tried various global minimization algorithms available in the scipy library but differential_evolution seems well enough suited to the problem
    #share_timeline.to_csv('timeline_output.csv')
    #print(share_timeline)
    return share_timeline.x


def debt_size_flat(cashflow, cod, debt_term):
    # primary repayment is flat across debt period
    # interest is decreasing
    # different to previous approaches of "sculpted" repayment period since this had the unrealistic effect of pushing all debt repayment
    # to the end of the repayment period where possible

    debt_term = debt_term  # adjust for python zero indexing

    cashflow['total_debt'] = 0
    cashflow['primary'] = 0
    cashflow['interest'] = 0
    cashflow['debt_after_pay'] = 0
    # flat debt repayment can be calculated analytically as below
    total_debt_service = cashflow['debt_service'][:debt_term].sum()
    # total debt service is equal to sum of primary and interest payments
    cashflow.at[:debt_term - 1, 'total_debt'] = (total_debt_service) / (1 + cod * (debt_term - 1) + 0.5 * cod * (debt_term - 1))
    # primary is equal to total debt divided by debt term
    cashflow.at[:debt_term - 1, 'primary'] = cashflow['total_debt'][0] / (debt_term - 1)
    # calculate debt remaining at start of every step - function only of year and total debt
    cashflow.at[:debt_term - 1, 'total_debt'] = cashflow['total_debt'][0] - cashflow['primary'][:debt_term] * (cashflow.index[:debt_term] - 1.0)
    cashflow.at[:debt_term - 1, 'interest'] = cashflow['total_debt'][:debt_term] * cod
    cashflow.at[:debt_term - 1, 'debt_after_pay'] = cashflow['total_debt'][:debt_term] - cashflow['primary'][:debt_term]

    return cashflow


def equity_irr_calculation_flat(share_timeline, start_year, cfd_dscr, debt_term, cod, p90_ratio, lifetime, capex,cfd_low_lin, cfd_central_lin, merchant_low_lin, merchant_central_lin, annual_generation,cost_input, merchant_dscr_input, merchant_input, cashflow_tail):
    # start year should be the same as for monthly optimisation - avoiding introducing a start month at the moment for simplicity
    # cfd_dscr is the debt service coverage ratio applied to cfd revenue component
    # debt_term is lenght of debt repayment
    # cod is cost of debt
    # p90 ratio is share of p50 generation under p90 generation scenario - may not be used here initially
    # lifetime is the operating lifetime of the asset

    # share_timeline = share_timeline.round(1)
    #print(share_timeline)
    #print(share_timeline.mean())
    merchant_share = share_timeline.mean()  # actually the cfd share at this point bc that's how merchant dscr is input
    merchant_share_int = np.floor(merchant_share*10) # actually the cfd share at this point bc that's how merchant dscr is input
    merchant_share_int = int(merchant_share_int)
    #print("merchant"+merchant_share_int)
    merchant_share = 1 - merchant_share
    merchant_dscr = merchant_dscr_input.iloc[merchant_share_int]['Merchant DSCR']
    

    # STEP ONE: SETUP CASHFLOWS

    cashflow = pd.DataFrame()
    cashflow['share'] = share_timeline
    cashflow['cfd_low'] = cfd_low_lin['gradient'] * cashflow['share'] + cfd_low_lin['intercept'] + cfd_low_lin['second_gradient'] * (cashflow['share'] ** 2) + cfd_low_lin['third_gradient'] * (cashflow['share'] ** 3)
    cashflow['cfd_central'] = cfd_central_lin['gradient'] * cashflow['share'] + cfd_central_lin['intercept'] + cfd_central_lin['second_gradient'] * (cashflow['share'] ** 2) + cfd_central_lin['third_gradient'] * (cashflow['share'] ** 3)
    cashflow['merchant_low'] = merchant_low_lin['gradient'] * cashflow['share'] + merchant_low_lin['intercept'] + merchant_low_lin['second_gradient'] * (cashflow['share'] ** 2) + merchant_low_lin['third_gradient'] * (cashflow['share'] ** 3)
    cashflow['merchant_central'] = merchant_central_lin['gradient'] * cashflow['share'] + merchant_central_lin['intercept'] + merchant_central_lin['second_gradient'] * (cashflow['share'] ** 2) + merchant_central_lin['third_gradient'] * (cashflow['share'] ** 3)
    cashflow['costs'] = cost_input.iloc[0:15, 1]
    # cashflow during the cfd period
    # print(merchant_dscr)

    # STEP TWO: CALCULATE DEBT SIZING

    cashflow['debt_service'] = cashflow['cfd_low'] / cfd_dscr + cashflow['merchant_low'] / merchant_dscr - cashflow['costs'] / ((merchant_dscr * merchant_share) + (cfd_dscr * (1 - merchant_share)))
    # debt service is cashflow available divided by dscr
    # debt service is cashflow available divided by dscr
    cashflow['total_debt'] = 0

    # total_debt_size = 0
    # total_debt_size = total_debt_size = fsolve(debt_size,capex,args=(cashflow,cod,debt_term),xtol=0.01)
    # fsolve finds total debt size so that primary + interest = money available for debt service in each year

    cashflow = debt_size_flat(cashflow, cod, debt_term)
    total_debt_size = cashflow['total_debt'][0]

    # STEP THREE: CALCULATE RETURNS ON EQUITY

    cashflow['equity_return'] = cashflow['cfd_central'] + cashflow['merchant_central'] - cashflow['costs'] - cashflow['primary'] - cashflow['interest']
    cashflow_tail['equity_return'] = cashflow_tail['central_revenue'] - cashflow_tail['costs']

    # print(cashflow)
    # print(cashflow_tail)

    total_cashflow = pd.DataFrame()
    total_cashflow['equity_net'] = np.zeros(lifetime)

    total_cashflow.at[0:15, 'equity_net'] = cashflow['equity_return']

    for i in np.arange((15 + 1), lifetime + (1)):
        total_cashflow.at[i - (1), 'equity_net'] = cashflow_tail['equity_return'][i]

    total_cashflow.at[0, 'equity_net'] = total_cashflow['equity_net'][0] - (capex - total_debt_size)
    ##debugging prints:
    #print('capex:', capex)
    #print('total debt', total_debt_size)
    #print('cashflow:','\n', cashflow)
    #print(total_cashflow)
    equity_irr = npf.irr(total_cashflow['equity_net'])
    #print('equity irr', equity_irr)

    #cashflow.to_csv('cashflow.csv')

    return (-1 * equity_irr)


######## BELOW IS THE PART OF THE SCRIPT WHICH LAUNCHES THE FUNCTIONS DEFINED ABOVE. NEEDS TO BE SETUP WITH
#######  REQUIRED FINANCING PARAMETERS/ CAPEX SHOULD BE ENTERED HERE
#######  OPTIMIZATION SCRIPT TAKES A WHILE
#######  THIS SCRIPT SHOULD BE USED FOR TIMELINE OPTIMISATION. IRR CALCULATION SCRIPT SHOULD THEN BE UES TO VALIDATE RESULTS

##Forum PPA: 
##monthly_opt = monthly_optimization(227.8,1,2022)

#share_timeline = timeline_optimization(2022,1.1,15,0.01,1,25,5845160)
#share_timeline=np.array([0,0,0,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5])
#equity_irr = equity_irr_calculation_setup(share_timeline.x,2022,1.1,15,0.01,1,25,5845160)
#print(share_timeline)
#print(-1*equity_irr)

#share_timeline = timeline_optimization(2022,1.2,15,0.01,1,25,5845160)
#share_timeline=np.array([0,0,0,0,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5])
#equity_irr = equity_irr_calculation_setup(share_timeline.x,2022,1.2,15,0.01,1,25,5845160)
#print(share_timeline)
#print(share_timeline.x)
#print(-1*equity_irr)


#share_timeline = np.array([0.0, 0.0, 0.0, 0.0 , 0.0,0.0, 0.0, 0.0, 0.0, 0.03,0.117, 0.135, 0.971 , 0.973, 0.961])
#equity_irr = equity_irr_calculation_setup(share_timeline,2022,1.2,15,0.01,1,30,2670972.50004565)
#print(-1*equity_irr)
#print(share_timeline.x.mean())
#print(share_timeline.x)

#share_timeline = np.array([0.00315194,0.00652349,0.0024826,0.00291685,0.00611632,0.15031227,0.03432245,0.08387779,0.5759326,0.96865867,0.95288267,0.99861536,0.98315146,0.98807536,0.99985767])
#share_timeline = share_timeline.round(1)
#equity_irr = equity_irr_calculation_setup(share_timeline,2022,1.2,15,0.01,1,30,2670972.50004565)
#equity_irr
##monthly_optimization(229.8,6,2023)
##share_timeline = timeline_optimization(start_year,cfd_dscr, debt_term, cod, p90_ratio, lifetime,capex)
##share_timeline = timeline_optimization(2022,1.1,15,0.02,1,25,5845160)

start_yr = 2022
row = start_yr - 2000 - 23 ## row indexed, better to use - 2022 -1 allow for ctrl+h
inputs = pd.read_csv("inputs/StrikePrice_Input.csv")
#print(inputs)
###capex_input = inputs.iloc[row,2]
###strike_price = inputs.iloc[row, 1]
#Igneo prices:
# 220 for Solar PV
# 200 for wind onshore

strike_price = 220
###print(start_yr, strike_price, capex_input)

monthly_opt = monthly_optimization(strike_price, 1, start_yr)
##share_timeline = timeline_optimization(start_yr,1.2,15,0.02,1,30,capex_input)
#share_timeline = np.array([0,0,0,0,0.08936805,0.17406847,0.18529904,0.23467969,0.2553131,0.26954818,0.28977298,0.32167185,0.36516977,0.39448969,0.45931851])
##equity_irr = equity_irr_calculation_setup(share_timeline,start_yr,1.2,15,0.02,1,30,capex_input)
print('finished',start_yr)
##print(share_timeline)
#print(share_timeline.x)
#share_timeline.to_csv('C:/Users/RafalMacuk/Documents/wonsz/PL CfD Opt 2204/CfD Python_final/outputs/timeline_',start_yr,'_',strike_price,'.csv')
##print(-1*equity_irr)

